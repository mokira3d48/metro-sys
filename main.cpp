# include <iostream>
# include <cmath>
# include <cstdio>

class Data {
  private :
    float **_d1;
    float **_d2;
    float *_md1;
    float *_md2;
    float *_e;
    float _me;

    int _row;
    int _col;
    int _e_count;

    float _h;
    float _st;

    // Methods : 
    float _dmin( int const, float*, float* );
    float _dmax( int const, float*, float* );
    float _min( int const, float* );
    float _max( int const, float* );
    float _abs( float const );

  public :
    // Getters :
    int rowCount() const;
    int colCount() const;
    int eCount() const;
    float h() const;
    float st() const;
    float meanE() const;

    // Constructor :
    Data( const int, const int, const int );

    // Methods :
    void setD1( int const, int const, float );
    void setD2( int const, int const, float );
    void setE( int const, float );

    // Operator :
    void calculate();

    // Destroyer :
    ~Data();

};

int Data::rowCount() const {
  return _row;

}
int Data::colCount() const {
  return _col;

}
int Data::eCount() const {
  return _e_count;

}

float Data::h() const {
  return _h;

}

float Data::st() const {
  return _st;

}

float Data::meanE() const {
  return _me;

}

Data::Data( const int n, const int  m, const int p ) : _row( n ), _col( m ), _e_count( p ) {
  _d1  = new float*[n];
  _d2  = new float*[n];
  _md1 = new float[n];
  _md2 = new float[n];
  _e  = new float[p];
  _me = 0.0f;

  int i = 0;
  int j = 0;

  while ( i < n ) {
    _d1[i] = new float[m];
    _d2[i] = new float[m];
    i ++;
  }

  return ;

}

void Data::setD1( int const i, int const j, float _v_ ) {
  if (_d1[i][j] !=  0.0f )
    _md1[i] = _md1[i] - _d1[i][j];

  _d1[i][j] = _v_;
  _md1[i] = _md1[i] + _d1[i][j];

  return ;

}

void Data::setD2( int const i, int const j, float _v_ ) {
  if (_d2[i][j] !=  0.0f )
    _md2[i] = _md2[i] - _d2[i][j];

  _d2[i][j] = _v_;
  _md2[i] = _md2[i] + _d2[i][j];

  return ;

}

void Data::setE( int const i, float const _v_ ) {
  _e[i] = _v_;
  return ;

}

float Data::_dmin( int const _n_, float* _t1_, float* _t2_ ) {
  float mi = _t1_[0] > _t2_[0] ? _t1_[0] : _t2_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t1_[i] < mi ) 
      mi = _t1_[i];

    if ( _t2_[i] < mi )
      mi = _t2_[i];

    i ++;
  }

  return mi;

}

float Data::_dmax( int const _n_, float* _t1_, float* _t2_ ) {
  float mx =  _t1_[0] > _t2_[0] ? _t1_[0] : _t2_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t1_[i] > mx )
      mx = _t1_[i];

    if ( _t2_[i] > mx )
      mx = _t2_[i];

    i ++;
  }

  return mx;

}

float Data::_min( int const _n_, float* _t_ ) {
  float mi =  _t_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t_[i] > mi )
      mi = _t_[i];

    i ++;
  }

  return mi;

}

float Data::_max( int const _n_, float* _t_ ) {
  float mx =  _t_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t_[i] > mx )
      mx = _t_[i];

    i ++;
  }

  return mx;

}

float Data::_abs( float const _x_ ) {
  return _x_ < 0.0f ? -1.0f * _x_ : _x_ ;

}

void Data::calculate() { 
  // Calculate of mean :
  int i = 0;

  while ( i < _row ) {
    _md1[i] = _md1[i] / _col;
    _md2[i] = _md2[i] / _col;

    i ++;
  }

  i = 0;

  while ( i < _e_count ) {
    _me = _me + _e[i];
    i ++;
  }

  _me = _me / _e_count;

  // Calculate of H and ST.
  float hmin  = _abs( _me - _dmin( _row, _md1, _md2 ) );
  float hmax  = _abs( _me - _dmax( _row, _md1, _md2 ) );
  float stmin = _abs( _me - _min( _e_count, _e ) );
  float stmax = _abs( _me - _max( _e_count, _e ) );

  
  _h  = hmin > hmax ? hmin : hmax;
  _st = stmin > stmax ? stmin : stmax;

  // Calculate of Uh and Ust.
  // 4.242640687f = 3 * sqrt(2)
//  _uh  = h / 4.242640687f; // erreur homogeniete
//  _ust = st / 4.242640687f;

  return ;
}



Data::~Data() {
  delete _e;

  int i = 0;
  int j = 0;

  while ( i < _row ) {
    delete _d1[i];
    delete _d2[i];

    i ++;
  }

  delete _d1;
  delete _d2;
  delete _md1;
  delete _md2;

  return ;

}

int main( void ) {
	Data data( 4, 10, 30 );
//	data.setD1();
//	data.setD2();
//	data.setE();
	int i = 0;

	data.setD1( 0, 0, 40.004 );
	data.setD1( 0, 1, 40.000 );
	data.setD1( 0, 2, 39.997 );
	data.setD1( 0, 3, 39.998 );
data.setD1( 0, 4, 39.997 );
data.setD1( 0, 5, 39.995 );
data.setD1( 0, 6, 39.994 );
data.setD1( 0, 7, 39.994 );
data.setD1( 0, 8, 39.995 );
data.setD1( 0, 9, 39.996 );

data.setD1( 1, 0, 40.003 );
data.setD1( 1, 1, 40.004 );
data.setD1( 1, 2, 40.004 );
data.setD1( 1, 3, 40.003 );
data.setD1( 1, 4, 40.003 );
data.setD1( 1, 5, 40.002 );
data.setD1( 1, 6, 40.002 );
data.setD1( 1, 7, 40.001 );
data.setD1( 1, 8, 40.000 );
data.setD1( 1, 9, 40.001 );

data.setD1( 2, 0, 39.997 );
data.setD1( 2, 1, 39.996 );
data.setD1( 2, 2, 39.995 );
data.setD1( 2, 3, 39.994 );
data.setD1( 2, 4, 39.993 );
data.setD1( 2, 5, 39.993 );
data.setD1( 2, 6, 39.995 );
data.setD1( 2, 7, 39.997 );
data.setD1( 2, 8, 39.999 );
data.setD1( 2, 9, 40.000 );

data.setD1( 3, 0, 39.999 );
data.setD1( 3, 1, 39.997 );
data.setD1( 3, 2, 39.997 );
data.setD1( 3, 3, 39.997 );
data.setD1( 3, 4, 39.999 );
data.setD1( 3, 5, 40.002 );
data.setD1( 3, 6, 40.002 );
data.setD1( 3, 7, 40.001 );
data.setD1( 3, 8, 40.000 );
data.setD1( 3, 9, 49.998 );
data.setD1( 3, 9, 39.300 );
data.setD1( 3, 9, 39.998 );

// e

data.setE( 0, 40.003 );
data.setE( 1, 40.005 );
data.setE( 2, 40.002 );
data.setE( 3, 39.996 );
data.setE( 4, 39.991 );
data.setE( 5, 39.990 );
data.setE( 6, 39.991 );
data.setE( 7, 39.994 );
data.setE( 8, 39.997 );
data.setE( 9, 39.998 );
data.setE( 10, 40.000 );
data.setE( 11, 40.003 );
data.setE( 12, 40.006 );
data.setE( 13, 40.007 );
data.setE( 14, 40.005 );
data.setE( 15, 40.002 );
data.setE( 16, 40.000 );
data.setE( 17, 39.999 );
data.setE( 18, 39.998 );
data.setE( 19, 39.997 );
data.setE( 20, 39.995 );
data.setE( 21, 39.994 );
data.setE( 22, 39.992 );
data.setE( 23, 39.991 );
data.setE( 24, 39.994 );
data.setE( 25, 39.996 );
data.setE( 26, 39.998 );
data.setE( 27, 39.999 );
data.setE( 28, 40.001 );
data.setE( 29, 40.003 );

// d2

data.setD2( 0, 0, 40.002 );
data.setD2( 0, 1, 40.002 );
data.setD2( 0, 2, 40.002 );
data.setD2( 0, 3, 40.002 );
data.setD2( 0, 4, 40.002 );
data.setD2( 0, 5, 40.002 );
data.setD2( 0, 6, 40.001 );
data.setD2( 0, 7, 39.999 );
data.setD2( 0, 8, 39.997 );
data.setD2( 0, 9, 39.999 );

data.setD2( 1, 0, 39.999 );
data.setD2( 1, 1, 39.997 );
data.setD2( 1, 2, 39.998 );
data.setD2( 1, 3, 39.997 );
data.setD2( 1, 4, 39.996 );
data.setD2( 1, 5, 39.998 );
data.setD2( 1, 6, 40.004 );
data.setD2( 1, 7, 40.007 );
data.setD2( 1, 8, 40.010 );
data.setD2( 1, 9, 40.010 );

data.setD2( 2, 0, 40.004 );
data.setD2( 2, 1, 40.002 );
data.setD2( 2, 2, 40.002 );
data.setD2( 2, 3, 40.002 );
data.setD2( 2, 4, 40.000 );
data.setD2( 2, 5, 40.000 );
data.setD2( 2, 6, 39.999 );
data.setD2( 2, 7, 39.999 );
data.setD2( 2, 8, 40.000 );
data.setD2( 2, 9, 40.002 );

data.setD2( 3, 0, 40.003 );
data.setD2( 3, 1, 40.001 );
data.setD2( 3, 2, 39.998 );
data.setD2( 3, 3, 39.998 );
data.setD2( 3, 4, 40.000 );
data.setD2( 3, 5, 40.002 );
data.setD2( 3, 6, 40.002 );
data.setD2( 3, 7, 40.002 );
data.setD2( 3, 8, 40.002 );
data.setD2( 3, 9, 40.001 );

/*std::cout << "D1 : " << std::endl;
data.printD1();
std::cout << "D2 : " << std::endl;
data.printD2();
std::cout << "E : " << std::endl;
data.printE();


data.calculate();
std::cout << "D1 : " << std::endl;
data.printMeanD1();

std::cout << "D1 : " << std::endl;
data.printMeanD2();*/

data.calculate();

std::cout << "Mean e : " << data.meanE() << std::endl;
std::cout << "H : " << data.h() << std::endl;
std::cout << "St : " << data.st() << std::endl;

	return 0;

}
