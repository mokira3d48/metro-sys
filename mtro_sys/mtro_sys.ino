# include <LiquidCrystal.h>

/* LCD var */
# define RS 0xB
# define E  0xA
# define D4 0x5
# define D5 0x4
# define D6 0x3
# define D7 0x2

/* BUTTON var */
# define BUTTON 0x7 //

/* TEMP Sensor */
# define S1 A0
# define S2 A1
# define S3 A2
# define S4 A3

/* Count Data */
# define ROW_COUNT 04
# define COL_COUNT 2
# define E_COUNT   6

/************************************** Pir sensor object ***********************************************************/
class Button {
  private :
    bool state0;
    bool state;
    bool ch_state;
    byte pin;

  public :
    // constructor :
    Button( byte );

    // methods :
    bool clicked();
    bool presence_not_detected();
    

};

Button::Button( byte p ) {
  this->pin    = p;
  this->state0 = 0;

  pinMode( pin, INPUT );
}

bool Button::clicked() {
  state = digitalRead( pin ) == LOW ;

  if ( !state0 && state ) {
    state0   = 1;
    ch_state = 1;
  }
  else
    ch_state = 0;

  state0 = state;

  return ch_state;
}

bool Button::presence_not_detected() {
      state = digitalRead( pin ) == LOW ;

      if ( state0 && state ) {
        state0   = 0;
        ch_state = 1;
      }
      else 
        ch_state = 0;

      return ch_state;
}
/**************************************LiquidCrystal LCD object ***********************************************************/
class LiquidCrystalLCD : public LiquidCrystal {
  private :
    byte _x;
    byte _y;
    int _delay;

    // methods :
    void __setCursorPosition( byte, byte );

  public :
    // getter :
    int get_delay();

    // setter :
    void set_delay( int );

    // constructor :
    LiquidCrystalLCD( byte, byte, byte, byte, byte, byte );

    // methods :
    void putStr( byte, byte, String const& );
    void showMessage( byte, String const&, byte, String const& );
};

int LiquidCrystalLCD::get_delay() {
  return _delay;
}

void LiquidCrystalLCD::set_delay( int d ) {
  _delay = d;
}


LiquidCrystalLCD::LiquidCrystalLCD( byte rs, byte e, byte d4, byte d5, byte d6, byte d7 ) : LiquidCrystal( rs, e, d4, d5, d6, d7 ) {
  this->_x = 0;
  this->_y = 0;
  this->_delay = 1000;

  return ;
}

void LiquidCrystalLCD::__setCursorPosition( byte x_val, byte y_val ) {
  this->_x = ( x_val < 0x2 ) ? x_val : 0x1 ;
  this->_y = ( y_val < 0x10 ) ? y_val : 0xF ;

  return ;
}

void LiquidCrystalLCD::putStr( byte x_val, byte y_val, String const& str ) {
  __setCursorPosition( x_val, y_val );
  setCursor( _y, _x );

  size_t l = str.length();
  size_t s =  _y + l;  

  if ( s < 0x10 ) {
    print( str );
    _y = s;
  }
  else {
    int k = 0x10 - _y;
    String es1 = str.substring( 0x0, k );
    String es2 = str.substring( k );

    print( es1 );
    
    if ( _x == 0x0 ) {
      setCursor( 0x0, 0x1 );
      print( es2 );
    }

    _x = 1;
    
  }

  return ;
}

void LiquidCrystalLCD::showMessage( byte y0, String const& str0, byte y1, String const& str1 ) {
  clear();
  putStr(0, y0, str0 );
  putStr(1, y1, str1 );

  if ( _delay > 0 ) delay( _delay );

  return ;
}

class Data {
  private :
    float **_d1;
    float **_d2;
    float *_md1;
    float *_md2;
    float *_e;
    float _me;

    int _row;
    int _col;
    int _e_count;

    float _h;
    float _st;

    // Methods : 
    float _dmin( int const, float*, float* );
    float _dmax( int const, float*, float* );
    float _min( int const, float* );
    float _max( int const, float* );
    float _abs( float const );

  public :
    // Getters :
    int rowCount() const;
    int colCount() const;
    int eCount() const;
    float h() const;
    float st() const;
    float meanE() const;

    // Constructor :
    Data( const int, const int, const int );

    // Methods :
    void setD1( int const, int const, float );
    void setD2( int const, int const, float );
    void setE( int const, float );

    // Operator :
    void calculate();

    // Destroyer :
    ~Data();

};

int Data::rowCount() const {
  return _row;

}
int Data::colCount() const {
  return _col;

}
int Data::eCount() const {
  return _e_count;

}

float Data::h() const {
  return _h;

}

float Data::st() const {
  return _st;

}

float Data::meanE() const {
  return _me;

}

Data::Data( const int n, const int  m, const int p ) : _row( n ), _col( m ), _e_count( p ) {
  _d1  = new float*[n];
  _d2  = new float*[n];
  _md1 = new float[n];
  _md2 = new float[n];
  _e  = new float[p];
  _me = 0.0f;

  int i = 0;
  int j = 0;

  while ( i < n ) {
    _d1[i] = new float[m];
    _d2[i] = new float[m];
    i ++;
  }

  return ;

}

void Data::setD1( int const i, int const j, float _v_ ) {
  if (_d1[i][j] !=  0.0f )
    _md1[i] = _md1[i] - _d1[i][j];

  _d1[i][j] = _v_;
  _md1[i] = _md1[i] + _d1[i][j];

  return ;

}

void Data::setD2( int const i, int const j, float _v_ ) {
  if (_d2[i][j] !=  0.0f )
    _md2[i] = _md2[i] - _d2[i][j];

  _d2[i][j] = _v_;
  _md2[i] = _md2[i] + _d2[i][j];

  return ;

}

void Data::setE( int const i, float const _v_ ) {
  _e[i] = _v_;
  return ;

}

float Data::_dmin( int const _n_, float* _t1_, float* _t2_ ) {
  float mi = _t1_[0] > _t2_[0] ? _t1_[0] : _t2_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t1_[i] < mi ) 
      mi = _t1_[i];

    if ( _t2_[i] < mi )
      mi = _t2_[i];

    i ++;
  }

  return mi;

}

float Data::_dmax( int const _n_, float* _t1_, float* _t2_ ) {
  float mx =  _t1_[0] > _t2_[0] ? _t1_[0] : _t2_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t1_[i] > mx )
      mx = _t1_[i];

    if ( _t2_[i] > mx )
      mx = _t2_[i];

    i ++;
  }

  return mx;

}

float Data::_min( int const _n_, float* _t_ ) {
  float mi =  _t_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t_[i] > mi )
      mi = _t_[i];

    i ++;
  }

  return mi;

}

float Data::_max( int const _n_, float* _t_ ) {
  float mx =  _t_[0];
  int    i = 1;

  while ( i < _n_ ) {
    if ( _t_[i] > mx )
      mx = _t_[i];

    i ++;
  }

  return mx;

}

float Data::_abs( float const _x_ ) {
  return _x_ < 0.0f ? -1.0f * _x_ : _x_ ;

}

void Data::calculate() { 
  // Calculate of mean :
  int i = 0;

  while ( i < _row ) {
    _md1[i] = _md1[i] / (float)_col;
    _md2[i] = _md2[i] / (float)_col;

    i ++;
  }

  i = 0;

  while ( i < _e_count ) {
    _me = _me + _e[i];
    i ++;
  }

  _me = _me / (float)_e_count;

  // Calculate of H and ST.
  float hmin  = _abs( _me - _dmin( _row, _md1, _md2 ) );
  float hmax  = _abs( _me - _dmax( _row, _md1, _md2 ) );
  float stmin = _abs( _me - _min( _e_count, _e ) );
  float stmax = _abs( _me - _max( _e_count, _e ) );

  
  _h  = hmin > hmax ? hmin : hmax;
  _st = stmin > stmax ? stmin : stmax;

  // Calculate of Uh and Ust.
  // 4.242640687f = 3 * sqrt(2)
//  _uh  = h / 4.242640687f; // erreur homogeniete
//  _ust = st / 4.242640687f;

  return ;
}



Data::~Data() {
  delete _e;

  int i = 0;
  int j = 0;

  while ( i < _row ) {
    delete _d1[i];
    delete _d2[i];

    i ++;
  }

  delete _d1;
  delete _d2;
  delete _md1;
  delete _md2;

  return ;

}

/*---------------------------------------------*/
LiquidCrystalLCD _lcd( RS, E, D4, D5, D6, D7 );
Button *_button;
Data* _data;

String __format_min_sec( int const _time_ ) {
  int minut = _time_ / 60;
  int sec   = _time_ - ( minut * 60 );

  return String( String( minut ) + "mm " + String( sec ) + "s" );
}

void __wait( int delay_time ) {
  int counter = delay_time;

  do {
    _lcd.showMessage( 0, "Start MetroSYS..", 0, "IN : " + __format_min_sec( counter ) );
    counter -- ;
  }
  while ( counter > 0 );

  _lcd.showMessage( 1, "MetroSYS", 5, "IS READY !" );

  return ;

}

float _readS( int const _index_ ) {
  int bv = 0;

  switch ( _index_ ) {
    case 0 : bv = analogRead( S1 ); break;// 0 - 1023
    case 1 : bv = analogRead( S2 ); break;// 0 - 1023
    case 2 : bv = analogRead( S3 ); break;// 0 - 1023
    case 3 : bv = analogRead( S4 ); break;// 0 - 1023
    default : break;
  }

  // conversion de valeur.

  return ( (float)bv ) * 100.0f / 1023.0f;
  //return ( (float)bv );
}

bool _reading;
int  _position; // 0 -> Waitting;  1 -> A1 ; 2 -> A2 ; 3 -> E ; 

int _j = 0;

void setup() {
  // Entrer des sondes de température :
  pinMode( S1, INPUT );
  pinMode( S2, INPUT );
  pinMode( S3, INPUT );
  pinMode( S4, INPUT );

  // LCD initialization ...
  _lcd.begin( 16, 2 );

  // Button initialization ...
  _button = new Button( BUTTON );

  // Initialisation de la mémoire de données ...
  _data = new Data( ROW_COUNT, COL_COUNT, E_COUNT );

  Serial.begin( 9600 );


  // We wait 30 min.
  //wait( 1800 );
  __wait( 30 );

  _reading  = 0;
  _position = 0;

}

void loop() {
  if ( _button->clicked() ) {
    _lcd.showMessage( 6, "Click ON", 0, "" );
    if ( !_reading ) {
      _reading = 1;
      _position ++;

      if ( _position == 4 ) {
        _lcd.showMessage( 2, "Calculate ...", 0, "" );
        _data->calculate();
        _lcd.showMessage( 0, String( _data->h() ), 0,  String( _data->st() ) );
        Serial.print("H = " );
        Serial.println(String(_data->h() ) );
        Serial.print("St = " );
        Serial.println(String(_data->st()) );
        Serial.print("Mean E = " );
        Serial.println(String(_data->meanE()) );
        

        _position = 0;
        _reading  = 0;
      }
    }
    else {
      _position = 0;
      _reading  = 0;

      _lcd.showMessage( 3, "Measurement", 0, "process cancel!" );
    }

    _j = 0;
  }

  switch( _position ) {
    case 1 :
      if ( _reading && _j < _data->colCount() ) {
        int i = 0;
        String m( "" );

        while ( i < _data->rowCount() ) {
          _data->setD1( i, _j, _readS( i ) );
          m += String((i + 1) + " : " + String(_readS(i) ) );
          i ++;
        }
        _lcd.showMessage( 0, "p = " + String(_position) + " j  = " + String(_j ), 0, m );
        _j ++;
      }
      else {
        _reading = 0;
        _lcd.showMessage( 0, "Click ON again", 0, "to continue." );
      }
      break;

    case 2 :
      if ( _reading && _j < _data->colCount() ) {
        int i = 0;
        String m( "" );

        while ( i < _data->rowCount() ) {
          _data->setD2( i, _j, _readS( i ) );
          m += String((i + 1) + " : " + String(_readS(i) ) );
          i ++;
        }

        _lcd.showMessage( 0, "p = " + String(_position) + " j  = " + String(_j ), 0, m );

        _j ++;
      }
      else {
        _reading = 0;
        _lcd.showMessage( 0, "Click ON again", 0, "to continue." );
      }
      break;

    case 3 :
    if ( _reading && _j < _data->eCount() ) { 
      _data->setE( _j, _readS( 0 ) );
      _lcd.showMessage( 0, "p = " + String(_position) + " j  = " + String(_j ), 0, "E : " +  String(_readS( 0 ) ) );
      _j ++;
    }
    else {
      _reading = 0;
      _lcd.showMessage( 0, "Click ON again", 0, "to calculate." );
    }
    break;

    default :break;
  }
  
}
